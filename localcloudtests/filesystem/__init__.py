import pathlib
import shutil
import subprocess

from ..common import CloudTester


class FilesystemTester(CloudTester):
    def __init__(self, image_path, mount_point):
        super().__init__()
        self.image_path = pathlib.Path(image_path)
        self.mount_point = pathlib.Path(mount_point)

    def connect(self):
        # setup a loop device, determine which was used
        status = subprocess.run(
            ['losetup', '-f', '-P', '--show', self.image_path],
            stdout=subprocess.PIPE,
        )

        self.loop_dev = status.stdout.decode().strip()
        if not self.loop_dev:
            raise ValueError('Failed to allocate a loop device')

        # assume partition 1; need to test with gpt/efi
        subprocess.run(['mount', '%sp1' % self.loop_dev, self.mount_point])

    def disconnect(self):
        subprocess.run(['umount', self.mount_point])
        subprocess.run(['losetup', '-d', self.loop_dev])

    def copy_image_test_code(self, filename):
        src = pathlib.Path('targetcloudtests').absolute()
        dst = self.mount_point.joinpath('targetcloudtests')
        shutil.rmtree(dst, ignore_errors=True)
        shutil.copytree(src, dst)

    def run_image_test_code(self, file_name):
        p = subprocess.Popen(
            ['chroot', self.mount_point, 'python3', '/targetcloudtests/test_static_image.py'],
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        stdout, stderr = p.communicate()

        print('Stdout:')
        print(stdout.decode('utf-8'))

        print('Stderr:')
        print(stderr.decode('utf-8'))

        return p.returncode == 0
