#! /usr/bin/python3

import grp
import pathlib
import pwd
import re
import spwd
import subprocess
import unittest

import apt
import aptsources
import aptsources.sourceslist


class NetworkingTestCase(unittest.TestCase):
    def setUp(self):
        super().setUp()
        cache = apt.Cache()
        package = cache["python3-psutil"]
        package.mark_install()
        package = cache["python3-netifaces"]
        package.mark_install()
        cache.commit()

    def test_open_ports(self):
        import netifaces
        import psutil

        connections = psutil.net_connections()
        ports = set()
        for c in connections:
            if c.status == 'LISTEN':
                ports.add(c.laddr[1])
            if c.type == 2 and c.status == 'NONE':  # 2 identifies UDP ports
                ports.add(c.laddr[1])

        # Port 22: ssh
        # Port 68: dhcp
        # Port 123: ntp
        # Port 546: dhcp ipv6
        self.assertTrue({22, 68}.issubset(ports), "We have not enough ports")
        self.assertTrue(ports.issubset({22, 68, 123, 546}), "We have too many ports")
        # self.assertSetEqual(ports, {'22', '68'}, "We have to many ports")


class PackagingTestCase(unittest.TestCase):
    def setUp(self):
        super().setUp()
        self.urls = set()
        self.distributions = set()
        self.components = set()
        apt_sources = aptsources.sourceslist.SourcesList()
        for entry in apt_sources:
            if not entry.disabled and entry.uri:
                self.urls.add(entry.uri)
                self.distributions.add(entry.dist)
                self.components = self.components.union(set(entry.comps))

    def test_distributions(self, distribution='stretch'):
        for d in self.distributions:
            self.assertTrue(d.startswith(distribution), "We should have only %s" % distribution)

    def test_urls(self):
        self.assertSetEqual(self.urls, {'http://security.debian.org',
                                        'http://cdn-aws.deb.debian.org/debian'},
                            "we have some strange mirror URLs")

    def test_components(self):
        self.assertSetEqual(self.components, {"main"}, "We should have only main")


class FilesystemTestCase(unittest.TestCase):
    def check_file(self, directory):
        temp_file_name = "temporary"
        temp_file_path = directory / temp_file_name
        content = "Ala ma kota"
        with temp_file_path.open("w") as file:
            file.write(content)
        with temp_file_path.open("r") as file:
            read_content = file.read()
        if content != read_content:
            print("Something is wrong")
        temp_file_path.unlink()

    def test_filesystems(self):
        self.check_file(pathlib.Path("~").expanduser())

    def test_tmp(self):
        self.check_file(pathlib.Path("/tmp"))
        self.check_file(pathlib.Path("/var/tmp"))


class UsersTestCase(unittest.TestCase):
    def check_user(self, username, uid, gid, homedir, shell="/bin/bash"):
        user = pwd.getpwnam(username)
        self.assertEqual(user.pw_uid, uid, "{0} should have uid {1}".format(username, uid))
        self.assertEqual(user.pw_dir, homedir, "{0} should have home {1}".format(username, homedir))
        self.assertEqual(user.pw_shell, "/bin/bash", "{0} should have shell {1}".format(username, shell))
        self.assertEqual(user.pw_gid, gid, "{0} should have gid {1}".format(username, gid))

        user = pwd.getpwuid(uid)
        self.assertEqual(user.pw_name, username, "user with uid {0} should be {1}".format(uid, username))
        group = grp.getgrnam(username)
        self.assertEqual(group.gr_gid, gid, "group for username {0} should have gid".format(username, gid))
        self.assertLessEqual(group.gr_mem, [], "We have to many group members")
        group = grp.getgrgid(gid)
        self.assertEqual(group.gr_name, username, "default should not have the same name as the user")

    def test_root(self):
        self.check_user("root", 0, 0, "/root")

    def test_nonlogin_users(self):
        for entry in pwd.getpwall():
            if entry[0] not in ('root', 'admin'):
                self.assertIn(entry[6], ('/usr/sbin/nologin',
                                         '/bin/false',
                                         '/bin/sync'))

    def test_groups(self):
        for entry in grp.getgrall():
            if entry.gr_mem:
                self.assertListEqual(entry.gr_mem, ['admin'],
                                     'Wrong group membership')

    def test_ordinary_user(self):
        self.check_user("admin", 1000, 1000, "/home/admin")

    def test_no_other_ordinary_users(self):
        for entry in pwd.getpwall():
            if entry.pw_uid < 1000:
                pass
            elif entry.pw_uid == 65534:
                self.assertEqual(entry.pw_name, "nobody", "nobody should have uid 65534")
            else:
                self.assertEqual(entry.pw_name, "admin", "The only ordinary user should be admin with uid 1000")
                self.assertEqual(entry.pw_uid, 1000, "The only ordinary user should be admin with uid 1000")

    def test_daemons(self):
        for entry in spwd.getspall():
            self.assertIn(entry.sp_pwdp, ("*", "!", "!!"))

    def test_system_user_ssh_keys(self):
        for entry in pwd.getpwall():
            if entry.pw_uid in (0, 1000):
                pass
            else:
                ssh_dir = pathlib.Path(entry.pw_dir) / ".ssh"
                self.assertFalse(ssh_dir.exists())

    def test_login_users_ssh_keys(self):
        root = pwd.getpwuid(0)
        root_auth_keys = pathlib.Path(root.pw_dir) / ".ssh/authorized_keys"
        self.assertTrue(root_auth_keys.exists())
        with root_auth_keys.open() as f:
            content = f.readlines()

        self.assertEqual(len(content), 1, "Unexpected ssh key found")
        root_key = content[0].split()[-2]

        admin = pwd.getpwuid(1000)
        admin_auth_keys = pathlib.Path(admin.pw_dir) / ".ssh/authorized_keys"
        self.assertTrue(admin_auth_keys.exists())
        with admin_auth_keys.open() as f:
            content = f.readlines()

        self.assertEqual(len(content), 1, "Unexpected ssh key found")
        admin_key = content[0].split()[-2]

        self.assertEqual(root_key, admin_key, "admin and root ssh keys are different")

    def test_sudo(self):
        self.assertEqual(subprocess.call(['/bin/su', '-l', 'admin', '-c' '/usr/bin/sudo /bin/true']), 0)


class LogsTestCase(unittest.TestCase):
    def check_log(self, directory):
        for child in directory.iterdir():
            if child.is_file():
                self.check_file(child)
            else:
                self.check_log(child)

    def check_file(self, fileobj):
        print("File: {0}".format(fileobj))
        with fileobj.open() as f:
            try:
                for line in f.readlines():
                    self.assertIsNone(re.search(r'\serror\W', line.lower()))
            except UnicodeDecodeError:
                pass            # binary files cannot be checked

    def test_logs(self):
        self.check_log(pathlib.Path("/var/log"))

    def test_installation_logs(self):
        p = pathlib.Path("/var/log/fai")
        self.assertFalse(p.exists(), "Clear installation logs")


def test_cloud_init():
    cloud_init_run = pathlib.Path("/var/run/cloud-init")
    cloud_init_lib = pathlib.Path("/var/lib/cloud-init")
    cloud_init_logs = pathlib.Path("/var/log/cloud-init")
    for child in cloud_init_lib.iterdir():
        if child.is_file():
            print(child)
    # TODO: check for errors
    for child in cloud_init_logs.iterdir():
        if child.is_file():
            print(child)
    # TODO: check for status and results
    for child in cloud_init_run.iterdir():
        if child.is_file():
            print(child)


def test_network_drivers():
    raise NotImplementedError("No idea how to do it right now. Provider specific")


class AptTests(unittest.TestCase):
    def setUp(self):
        self.cache = apt.Cache()
        self.cache.update()
        self.cache.commit()
        self.cache.open(None)

    def test_installation(self):
        package_name = "git"
        package = self.cache[package_name]
        package.mark_install()
        self.cache.commit()          # todo: unittest warns about unclosed files

    def test_upgrade(self):
        self.cache.upgrade(True)
        self.cache.commit()          # todo: check install progress reaches 100%

        self.assertTrue(True)

    def tearDown(self):
        self.cache.close()
        self.cache = None

if __name__ == '__main__':
    unittest.main(warnings="ignore")
    # test_installation()
    # print("Packages installation tested")
    # test_filesystems()
    # print("Home directory R/W")
    # test_temporary_directories()
    # print("Temporary directories R/W")
    # test_log_directories()
    # print("Log directories OK")
    # test_users()
    # print("We have some users ;-)")
    # try:
    #     test_cloud_init()
    #     print("Cloud init is")
    # except:
    #     print("Problems with cloud init")
    # check_sources_list()
    # print("apt sources lists")
