import os
import pathlib
import unittest


class FilesystemTests(unittest.TestCase):
    def test_for_expected_logs(self):
        expected = {
            'files': set((
                'alternatives.log',
                'bootstrap.log',
                'btmp',
                'dpkg.log',
                'faillog',
                'install_packages.list',
                'lastlog',
                'tallylog',
                'wtmp',
            )),
            'dirs': set((
                'apt',
                'chrony',
                'fai',
                'unattended-upgrades',
            )),
            'symlinks': set(),
            'others': set(),
        }

        actual = {
            'files': set(),
            'dirs': set(),
            'symlinks': set(),
            'others': set(),
        }

        for i in os.scandir('/var/log'):
            if i.is_file(follow_symlinks=False):
                actual['files'].add(i.name)
            elif i.is_dir(follow_symlinks=False):
                actual['dirs'].add(i.name)
            elif i.is_symlink():
                actual['symlinks'].add(i.name)
            else:
                actual['others'].add(i.name)

        for kind in expected:
            # look for missing entries
            self.assertEqual(
                expected[kind] - actual[kind],
                set(),
                'expected %s missing in /var/log' % kind,
            )

            # look for unexpected entries
            self.assertEqual(
                actual[kind] - expected[kind],
                set(),
                'unexpected %s found in /var/log' % kind,
            )

    def test_kernel_and_initrd_exist(self):
        for fname in ('/vmlinuz', '/initrd.img'):
            # check that the entry in / is a symlink
            sym = pathlib.Path(fname)
            self.assertTrue(sym.exists(), '%s does not exist' % fname)
            self.assertTrue(sym.is_symlink(), '%s is not a symlink' % fname)

            # check that it's not a dangling symlink
            target = sym.resolve()
            self.assertTrue(target.exists(),
                            '%s symlink target is missing' % fname)

if __name__ == '__main__':
    unittest.main(warnings='ignore')
